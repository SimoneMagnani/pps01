package lab01.example.model;

public class SimpleBankAccountWithAtm extends AbstractBankAccount implements BankAccountWithAtm {

    private final double fee;

    public SimpleBankAccountWithAtm(final AccountHolder holder, final double balance,final double fee) {
        super(holder, balance);
        this.fee = fee;
    }

    @Override
    public void depositWithAtm(int usrID, double amount) {
        if (this.isDepositWithAtmAllowed(amount)) {
            this.deposit(usrID, amount);
            this.payFee(usrID);
        }
    }

    @Override
    public void withdrawWithAtm(int usrID, double amount) {
        if (this.isWithdrawWithAtmAllowed(amount)){
            this.withdraw(usrID, amount);
            this.payFee(usrID);
        }
    }

    private boolean isWithdrawWithAtmAllowed(final double amount){
        return this.balance+this.fee >= amount;
    }

    private boolean isDepositWithAtmAllowed(final double amount){
        return this.balance+amount-this.fee >= 0;
    }

    private void payFee(int usrID) {
        if (this.checkUser(usrID)) {
            this.balance -= this.fee;
        }
    }

}
